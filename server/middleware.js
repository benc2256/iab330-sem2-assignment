const bp = require('body-parser');

const errorMessages = function (req, res, next) {
	const error = (res, error, message) => res.status(error).send({ error, message: message || '' });

	res.throwNotFound = (message) => error(res, 404, message || `'${req.path}' invalid`);
	res.throwBadRequest = (message) => error(res, 400, message || 'Bad request');
	res.throwInternalError = (message) => error(res, 500, message || 'Internal server error');

	next();
};

module.exports = function (app) {
	app.use(bp.json());
	app.use(errorMessages);
};
