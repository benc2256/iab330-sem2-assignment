const express = require('express');
const mongoose = require('mongoose');
const cfg = require('./config');
const middleware = require('./middleware');
const router = require('./router');

// connect to mongo
mongoose.connect(cfg().db.connectionString, {
	useNewUrlParser: true
}).then(function () {
	console.log('established connection to mongodb');
}).catch(function (reason) {
	console.error('failed to connect to mongodb', reason);
})

// init
const app = express();
middleware(app); // set middlewares
router(app); // apply routes

const port = process.env.PORT || 3000;
app.listen(port, function (err) {
	if (err) {
		console.error('Failed to init server');
	} else {
		console.log('Server listening on port ' + port.toString());
	}
});