const mongoose = require('mongoose');
const { Types } = mongoose.Schema;

const ProductModelSchema = new mongoose.Schema({

	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: false
	},
	category: {
		type: Types.ObjectId,
		ref: 'Category'
	},
	productLine: {
		type: Types.ObjectId,
		ref: 'ProductLine'
	},
	extraInfo: {
		type: String,
		required: false
	},
	media: {
		type: String,
		required: false
	}

});
const ProductModel = mongoose.model('Product', ProductModelSchema);

module.exports = ProductModel;