const mongoose = require('mongoose');

const CategoryModelSchema = new mongoose.Schema({

	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: false
	},
	media: {
		type: String,
		required: false
	}

});
const CategoryModel = mongoose.model('Category', CategoryModelSchema);

module.exports = CategoryModel;