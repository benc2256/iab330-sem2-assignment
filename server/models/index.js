const UserModel = require('./user.model'),
	BrandModel = require('./brand.model'),
	CategoryModel = require('./category.model'),
	ProductLineModel = require('./product-line.model'),
	ProductModel = require('./product.model'),
	ReviewModel = require('./review.model');

module.exports = { UserModel, BrandModel, CategoryModel, ProductLineModel, ProductModel, ReviewModel };