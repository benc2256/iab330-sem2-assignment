const mongoose = require('mongoose');
const { Types } = mongoose.Schema;

const ProductLineModelSchema = new mongoose.Schema({

	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: false
	},
	media: {
		type: String,
		required: false
	},
	brand: {
		type: Types.ObjectId,
		ref: 'Brand',
		required: true
	}

});
const ProductLineModel = mongoose.model('ProductLine', ProductLineModelSchema);

module.exports = ProductLineModel;