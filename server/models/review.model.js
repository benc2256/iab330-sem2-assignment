const mongoose = require('mongoose');
const { Types } = mongoose.Schema;

const ReviewModelSchema = new mongoose.Schema({

	dateCreated: {
		type: Date,
		required: true
	},
	dateModified: {
		type: Date,
		required: false
	},
	rating: {
		type: Number,
		required: true
	},
	content: {
		type: String,
		required: true
	},
	media: {
		type: String,
		required: false
	},
	user: {
		type: Types.ObjectId,
		ref: 'User',
		required: false
	},
	product: {
		type: Types.ObjectId,
		ref: 'Product',
		required: true
	}

});
const ReviewModel = mongoose.model('Review', ReviewModelSchema);

module.exports = ReviewModel;