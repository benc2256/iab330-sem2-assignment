const mongoose = require('mongoose');

const UserModelSchema = new mongoose.Schema({

	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	firstName: {
		type: String,
		required: true
	},
	lastName: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	}

});
const UserModel = mongoose.model('User', UserModelSchema);

module.exports = UserModel;