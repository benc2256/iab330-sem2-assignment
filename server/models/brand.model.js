const mongoose = require('mongoose');
const { Types } = mongoose.Schema;

const BrandModelSchema = new mongoose.Schema({

	legalName: {
		type: String,
		required: true
	},
	tradingName: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: false
	},
	establishmentDate: {
		type: Date,
		required: true
	},
	media: {
		type: String,
		required: false
	},

});
const BrandModel = mongoose.model('Brand', BrandModelSchema);

module.exports = BrandModel;