const md5 = require('md5'),
	{ ObjectId } = require('mongoose').Types,
	cfg = require('../config'),
	{ UserModel } = require('../models');

const getPasswordHash = function (username, password) {
	return md5(cfg().passwordSalt + password);
};

module.exports.userGet = function (req, res) {
	UserModel.find({}).exec(function (err, results) {
		if (!!err) {
			res.throwInternalError();
		} else {
			res.status(200).send(results);
		}
	})
};

module.exports.userGetOne = function (req, res) {
	const { id } = req.params;

	if (ObjectId.isValid(id)) {
		UserModel.findOne({ _id: id }).exec(function (err, result) {
			if (!!err || !result) {
				res.throwBadRequest('User does not exist');
			} else {
				res.status(200).send(result);
			}
		});
	} else {
		res.throwBadRequest();
	}
};

module.exports.userPost = function (req, res) {
	const body = req.body;

	// perform validation
	if (!!body && !!body.firstName && !!body.lastName && !!body.username && !!body.password && !!body.email) {

		body.password = getPasswordHash(body.username, body.password);

		const user = new UserModel(body);
		user.save(function (err) {
			if (!!err) {
				res.throwInternalError();
			} else {
				res.status(200).send(user);
			}
		});

	} else {
		res.throwBadRequest();
	}
};