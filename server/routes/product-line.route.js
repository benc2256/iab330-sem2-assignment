const { ObjectId } = require('mongoose').Types,
	cfg = require('../config'),
	{ ProductLineModel, BrandModel } = require('../models');

module.exports.productLineGet = function (req, res) {
	ProductLineModel.find({}).exec(function (err, results) {
		if (!!err) {
			res.throwInternalError();
		} else {
			res.status(200).send(results);
		}
	})
};

module.exports.productLineGetOne = function (req, res) {
	const { id } = req.params;

	if (ObjectId.isValid(id)) {
		ProductLineModel.findOne({ _id: id }).exec(function (err, result) {
			if (!!err || !result) {
				res.throwBadRequest('Product line does not exist');
			} else {
				res.status(200).send(result);
			}
		});
	} else {
		res.throwBadRequest();
	}
};

module.exports.productLinePost = function (req, res) {
	const body = req.body;

	// perform validation
	// name description media brand
	if (!!body && !!body.name && !!body.brand) {

		if (!ObjectId.isValid(body.brand)) {
			res.throwBadRequest("Invalid brand ID");
			return;
		}

		// check the brand exists...
		BrandModel.find({ _id: body.brand }).exec(function (err, brand) {
			if (!!err || !brand) {
				res.throwBadRequest(`Brand with id ${body.brand} does not exist`);
			} else {

				const productLine = new ProductLineModel(body);
				productLine.save(function (err) {
					if (!!err) {
						res.throwInternalError();
					} else {
						res.status(200).send(productLine);
					}

				});
			}
		});

	} else {
		res.throwBadRequest();
	}
};

module.exports.productLineByBrandId = function (req,res){
	const{id} =req.params;
	if(ObjectId.isValid(id)){
		//Find all product lines that have brand ids that match the brand
		ProductLineModel.find({brand: id}).exec(function(err, results){
			//Check if there are any errors
			if(!!err){
				res.throwInternalError();
			} else {
				//Send the results
				res.status(200).send(results);
			}
		});
	} else {
		res.throwBadRequest();
	}
};