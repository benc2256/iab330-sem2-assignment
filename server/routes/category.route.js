const { ObjectId } = require('mongoose').Types,
	cfg = require('../config'),
	{ CategoryModel } = require('../models');

module.exports.categoryGet = function (req, res) {
	CategoryModel.find({}).exec(function (err, results) {
		if (!!err) {
			res.throwInternalError();
		} else {
			res.status(200).send(results);
		}
	})
};

module.exports.categoryGetOne = function (req, res) {
	const { id } = req.params;

	if (ObjectId.isValid(id)) {
		CategoryModel.findOne({ _id: id }).exec(function (err, result) {
			if (!!err || !result) {
				res.throwBadRequest('Category does not exist');
			} else {
				res.status(200).send(result);
			}
		});
	} else {
		res.throwBadRequest();
	}
};

module.exports.categoryPost = function (req, res) {
	const body = req.body;

	// perform validation
	if (!!body && !!body.name) {

		const category = new CategoryModel(body);
		category.save(function (err) {
			if (!!err) {
				res.throwInternalError();
			} else {
				res.status(200).send(category);
			}
		});

	} else {
		res.throwBadRequest();
	}
};