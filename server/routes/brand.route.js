const { ObjectId } = require('mongoose').Types,
	cfg = require('../config'),
	{ BrandModel } = require('../models');

module.exports.brandGet = function (req, res) {
	BrandModel.find({}).exec(function (err, results) {
		if (!!err) {
			res.throwInternalError();
		} else {
			res.status(200).send(results);
		}
	})
};

module.exports.brandGetOne = function (req, res) {
	const { id } = req.params;

	if (ObjectId.isValid(id)) {
		BrandModel.findOne({ _id: id }).exec(function (err, result) {
			if (!!err || !result) {
				res.throwBadRequest('Brand does not exist');
			} else {
				res.status(200).send(result);
			}
		});
	} else {
		res.throwBadRequest();
	}
};

module.exports.brandPost = function (req, res) {
	const body = req.body;

	// perform validation
	if (!!body && !!body.legalName && !!body.tradingName && !!body.establishmentDate) {

		const brand = new BrandModel(body);
		brand.save(function (err) {
			if (!!err) {
				res.throwInternalError();
			} else {
				res.status(200).send(brand);
			}
		});

	} else {
		res.throwBadRequest();
	}
};