const { ObjectId } = require('mongoose').Types,
	cfg = require('../config'),
	{ ReviewModel } = require('../models');

module.exports.reviewGet = function (req, res) {
	ReviewModel.find({}).exec(function (err, results) {
		if (!!err) {
			res.throwInternalError();
		} else {
			res.status(200).send(results);
		}
	})
};

module.exports.reviewGetOne = function (req, res) {
	const { id } = req.params;

	if (ObjectId.isValid(id)) {
		ReviewModel.findOne({ _id: id }).exec(function (err, result) {
			if (!!err || !result) {
				res.throwBadRequest('Review does not exist');
			} else {
				res.status(200).send(result);
			}
		});
	} else {
		res.throwBadRequest();
	}
};

module.exports.reviewPost = function (req, res) {
	const { dateCreated, rating, content, product } = req.body;

	const body = { dateCreated, rating, content, product };

	// perform validation
	if (dateCreated && rating && content && product) {

		const review = new ReviewModel(body);
		review.save(function (err) {
			if (!!err) {
				res.throwInternalError();
			} else {
				res.status(200).send(review);
			}
		});

	} else {
		res.throwBadRequest(JSON.stringify(body));
	}
};

module.exports.reviewGetByProduct = function (req,res){
	const{id} = req.params;

	if(ObjectId.isValid(id)){
		//Find all reviews that have a product matching the id 
		ReviewModel.find({product: id}).exec(function (err, result){
			//Check for any errors in the request
			if(!!err){
				res.throwBadRequest("Bad request");
			} else {
				//Send the results
				res.status(200).send(result);
			}
		});
	} else {
		res.throwBadRequest();
	}
};