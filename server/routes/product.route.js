const { ObjectId } = require('mongoose').Types,
	cfg = require('../config'),
	{ ProductModel, ProductLineModel } = require('../models');

module.exports.productGet = function (req, res) {
	ProductModel.find({}).exec(function (err, results) {
		if (!!err) {
			res.throwInternalError();
		} else {
			res.status(200).send(results);
		}
	})
};

module.exports.productGetOne = function (req, res) {
	const { id } = req.params;

	if (ObjectId.isValid(id)) {
		ProductModel.findOne({ _id: id }).exec(function (err, result) {
			if (!!err || !result) {
				res.throwBadRequest('Product does not exist');
			} else {
				res.status(200).send(result);
			}
		});
	} else {
		res.throwBadRequest();
	}
};

module.exports.productPost = function (req, res) {
	const body = req.body;

	// perform validation
	if (!!body && !!body.name) {

		const product = new ProductModel(body);
		product.save(function (err) {
			if (!!err) {
				res.throwInternalError();
			} else {
				res.status(200).send(product);
			}
		});

	} else {
		res.throwBadRequest();
	}
};

module.exports.productGetByBrand = function(req, res){
	const {id} = req.params;
	var productLines = [];

	if(ObjectId.isValid(id)){
		//Find the product lines that are linked to the brand
		ProductLineModel.find({brand: id}).exec(function (err,results){
			//Check if there are any errors in the request
			if(!!err){
				res.throwBadRequest("Bad request");
			} else {
				//Convert the productlines found to an array of their ids
				productLines = results.map(a => a._id);
				//Finds all products have product lines within the array of ids
				ProductModel.find({productLine: {$in: productLines}}).exec(function(err, results){
					if(!!err){
						res.throwBadRequest();
					} else {
						//send the results
						res.status(200).send(results);
					}
				});
			}
		});
	} else {
		res.throwBadRequest();
	}

};