const express = require('express');

const routes = {
	user: require('./routes/user.route'),
	category: require('./routes/category.route'),
	brand: require('./routes/brand.route'),
	productLine: require('./routes/product-line.route'),
	review: require('./routes/review.route'),
	product: require('./routes/product.route')
}

module.exports = function (app) {

	const api = express.Router();

	api.get('/user', routes.user.userGet);
	api.get('/user/:id', routes.user.userGetOne);
	api.post('/user', routes.user.userPost);

	api.get('/category', routes.category.categoryGet);
	api.get('/category/:id', routes.category.categoryGetOne);
	api.post('/category', routes.category.categoryPost);

	api.get('/brand', routes.brand.brandGet);
	api.get('/brand/:id', routes.brand.brandGetOne);
	api.post('/brand', routes.brand.brandPost);

	api.get('/product-line', routes.productLine.productLineGet);
	api.get('/product-line/brand/:id', routes.productLine.productLineByBrandId);
	api.get('/product-line/:id', routes.productLine.productLineGetOne);
	api.post('/product-line', routes.productLine.productLinePost);

	api.get('/review', routes.review.reviewGet);
	api.get('/review/:id', routes.review.reviewGetOne);
	api.post('/review', routes.review.reviewPost);
	api.get('/review/product/:id', routes.review.reviewGetByProduct);

	api.get('/product', routes.product.productGet);
	api.get('/product/brand/:id', routes.product.productGetByBrand);
	api.get('/product/:id', routes.product.productGetOne);
	api.post('/product', routes.product.productPost);

	app.use('/api', api);

	app.all('*', (req, res) => res.throwNotFound());

};