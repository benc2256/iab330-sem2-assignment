﻿using NUnit.Framework;
using SupplyReview.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplyReview.Tests.UnitTests.ViewModels
{
    [TestFixture]
    class TestProductListViewModel
    {
        // Test Data //----


        // Testing //----

        [SetUp]
        public void BeforeTestSuite()
        {
            // Do stuff here maybe? If needed.
        }


        [Test]
        public void CanInstantiateViewModel()
        {
            ProductListViewModel testViewModel = new ProductListViewModel(null);

            Assert.Pass("Your first passing test");
        }


        [TearDown]
        public void Cleanup()
        {
            // Remove all settings configuration
            // aka. reset everything to default
            Settings.ClearEverything();
        }
    }
}
