﻿using Moq;
using NUnit.Framework;
using SupplyReview.Models;
using SupplyReview.Services;
using SupplyReview.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SupplyReview.Tests.UnitTests.ViewModels
{
    [TestFixture]
    public class TestProductLineIndexViewModel
    {

        // Test Data //----

        /// <summary>
        /// Useful when needing to test a single list and filtering
        /// </summary>
        static object[] PRODUCT_LINE_LIST_TEST_DATA_03 =
        {
            new object [] // Case 1
            {
                new List<ProductLineModel>
                {
                    { new ProductLineModel() {_id = "1", Brand = "Vegi", Description = "Starch", Media = "http://www.look-at-me.com", Name = "Potato" } },
                    { new ProductLineModel() {_id = "2", Brand = "Apple", Description = "Is this copyright infringement?", Media = "http://www.look-at-me.com", Name = "iPhone" } }
                }
            },
            new object[] // Case 2
            {
                new List<ProductLineModel>
                {
                    { new ProductLineModel() {_id = "1", Brand = "Fruit", Description = "A colour and a flavour!", Media = "http://www.blind-as-a-bat.com", Name = "Orange" } },
                    { new ProductLineModel() {_id = "2", Brand = "ABC", Description = "In pyjamas", Media = "http://www.blind-as-a-bat.com", Name = "Bannana" } },
                    { new ProductLineModel() {_id = "3", Brand = "Fruit", Description = "Under the sea!", Media = "http://www.blind-as-a-bat.com", Name = "PineApple" } }
                }
            }
        };

        // Testing //----

        [SetUp]
        public void BeforeTestSuite()
        {
            // Do stuff here maybe? If needed.
        }

        /// <summary>
        /// Need to be able to instantiate view model with empty mock.
        /// </summary>
        [Test]
        public void CanInstantiateViewModelWithEmptyMock()
        {
            // Arrange
            var mockServiceRep = new Mock<IProductLineService>();

            mockServiceRep.SetupGet(service => service.ProductLines).Returns(new List<ProductLineModel>());
            //mockService.Setup(service => service.ProductLines).Returns(new List<ProductLineModel>());
            //mockService.SetupProperty(service => service.ProductLines, new List<ProductLineModel>());
            // The above all works btw, different methods to acheive the same effect

            var mockService = mockServiceRep.Object;

            // Act
            var testViewModel = new ProductLineIndexViewModel(mockService);

            // Assert
            // Postconditions
            Assert.AreEqual(mockService.ProductLines, testViewModel.ListViewModel.ProductLineList);
            Assert.IsEmpty(mockService.ProductLines);
            Assert.IsEmpty(testViewModel.ListViewModel.ProductLineList);
        }


        /// <summary>
        /// Need to be able to instantiate view model with data.
        /// </summary>
        /// <param name="testData"></param>
        [Test, TestCaseSource("PRODUCT_LINE_LIST_TEST_DATA_03")]
        public void CanInstantiateViewModelWithPopulatedMock(List<ProductLineModel> testData)
        {
            // Arrange
            var mockServiceRep = new Mock<IProductLineService>();
            mockServiceRep.SetupGet(service => service.ProductLines).Returns(testData);

            var mockService = mockServiceRep.Object;

            // Act
            var testViewModel = new ProductLineIndexViewModel(mockService);

            // Assert
            // Postconditions
            Assert.AreEqual(testData, mockService.ProductLines);
            Assert.AreEqual(mockService.ProductLines, testViewModel.ListViewModel.ProductLineList);
        }


        /// <summary>
        /// For this test to pass, each brand in the test set is filtered
        /// on, and the viewmodel must correctly filter the test data.
        /// </summary>
        /// <param name="testData"></param>
        [Test, TestCaseSource("PRODUCT_LINE_LIST_TEST_DATA_03")]
        public void CanViewModelFilterOnBrandCorrectly(List<ProductLineModel> testData)
        {
            // Preconditions
            Assert.IsNotEmpty(testData);

            // Arrange
            var mockServiceRep = new Mock<IProductLineService>();
            mockServiceRep.SetupGet(service => service.ProductLines).Returns(testData);

            var mockService = mockServiceRep.Object;
            var testViewModel = new ProductLineIndexViewModel(mockService);

            var brands = testData.Select(productLine => productLine.Brand).Distinct().ToList();
            foreach(var brand in brands)
            {
                // Act
                testViewModel.SetFilterOnBrand(new BrandModel() { _id = brand });

                // Assert
                var expected = testData.Where(productLine => productLine.Brand == brand).ToList();
                Assert.AreEqual(expected, testViewModel.ListViewModel.ProductLineList, "Failure on brand: " + brand);
            }

        }


        // Need a test for searching - making sure search works correctly




        [TearDown]
        public void Cleanup()
        {
            // Remove all settings configuration
            // aka. reset everything to default
            Settings.ClearEverything();
        }
    }
}