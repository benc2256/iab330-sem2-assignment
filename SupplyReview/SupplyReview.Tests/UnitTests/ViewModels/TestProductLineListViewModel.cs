﻿using NUnit.Framework;
using SupplyReview.Models;
using SupplyReview.Services;
using SupplyReview.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplyReview.Tests.UnitTests.ViewModels
{
    [TestFixture]
    class TestProductLineListViewModel
    {

        // Test Data //----

        /// <summary>
        /// Useful when needing to test a single list
        /// </summary>
        static object[] PRODUCT_LINE_LIST_TEST_DATA_01 =
        {
            new object [] // Case 1
            {
                new List<ProductLineModel>
                {
                    { new ProductLineModel() {_id = "1", Brand = "Vegi", Description = "Starch", Media = "http://www.look-at-me.com", Name = "Potato" } },
                    { new ProductLineModel() {_id = "2", Brand = "Apple", Description = "Is this copyright infringement?", Media = "http://www.look-at-me.com", Name = "iPhone" } }
                }
            },
            new object[] // Case 2
            {
                new List<ProductLineModel>
                {
                    { new ProductLineModel() {_id = "1", Brand = "Fruit", Description = "A colour and a flavour!", Media = "http://www.blind-as-a-bat.com", Name = "Orange" } },
                    { new ProductLineModel() {_id = "2", Brand = "ABC", Description = "In pyjamas", Media = "http://www.blind-as-a-bat.com", Name = "Bannana" } }
                }
            }
        };

        /// <summary>
        /// Useful when needing to test switching between two lists
        /// </summary>
        static object[] PRODUCT_LINE_LIST_TEST_DATA_02 =
        {
            new object [] // Case 1
            {
                new List<ProductLineModel>
                {
                    { new ProductLineModel() {_id = "1", Brand = "Vegi", Description = "Starch", Media = "http://www.look-at-me.com", Name = "Potato" } },
                    { new ProductLineModel() {_id = "2", Brand = "Apple", Description = "Is this copyright infringement?", Media = "http://www.look-at-me.com", Name = "iPhone" } }
                },
                new List<ProductLineModel>
                {
                    { new ProductLineModel() {_id = "1", Brand = "Fruit", Description = "A colour and a flavour!", Media = "http://www.blind-as-a-bat.com", Name = "Orange" } },
                    { new ProductLineModel() {_id = "2", Brand = "ABC", Description = "In pyjamas", Media = "http://www.blind-as-a-bat.com", Name = "Bannana" } }
                }
            },
            new object[] // Case 2
            {
                new List<ProductLineModel>
                {
                    { new ProductLineModel() {_id = "1", Brand = "Fruit", Description = "A colour and a flavour!", Media = "http://www.blind-as-a-bat.com", Name = "Orange" } },
                    { new ProductLineModel() {_id = "2", Brand = "ABC", Description = "In pyjamas", Media = "http://www.blind-as-a-bat.com", Name = "Bannana" } }
                },
                new List<ProductLineModel>
                {
                    { new ProductLineModel() {_id = "1", Brand = "Vegi", Description = "Starch", Media = "http://www.look-at-me.com", Name = "Potato" } },
                    { new ProductLineModel() {_id = "2", Brand = "Apple", Description = "Is this copyright infringement?", Media = "http://www.look-at-me.com", Name = "iPhone" } }
                }
            }
        };


        // Testing //----

        [SetUp]
        public void BeforeTestSuite()
        {
            // Do stuff here maybe? If needed.
        }


        /// <summary>
        /// Need to be able to instantiate view model to pass this test.
        /// </summary>
        [Test]
        public void CanInstantiateViewModel()
        {
            ProductLineListViewModel testViewModel = new ProductLineListViewModel(null);

            Assert.Pass("Your first passing test");
        }


        /// <summary>
        /// Must be able to successfully assign a list to the view model to pass this test.
        /// </summary>
        /// <param name="testData">The test data</param>
        [Test, TestCaseSource("PRODUCT_LINE_LIST_TEST_DATA_01")]
        public void CanAssignAListToViewModel(List<ProductLineModel> testData)
        {
            // Preconditions
            Assert.NotNull(testData);

            // Init
            ProductLineListViewModel testViewModel = new ProductLineListViewModel(new ObservableCollection<ProductLineModel>(testData));

            // Postconditions
            Assert.NotNull(testViewModel.ProductLineList);
            Assert.AreEqual(testData, testViewModel.ProductLineList); // Despite being different types of collections, they contain same data
            Assert.AreNotSame(testData, testViewModel.ProductLineList);
        }


        /// <summary>
        /// Must be able to refresh data using refresh data method.
        /// </summary>
        /// <param name="testDataList1">The list to init with.</param>
        /// <param name="testDataList2">The list to refresh with.</param>
        [Test, TestCaseSource("PRODUCT_LINE_LIST_TEST_DATA_02")]
        public void CanRefreshListOfViewModel(List<ProductLineModel> testDataList1, List<ProductLineModel> testDataList2)
        {
            // Preconditions
            Assert.NotNull(testDataList1);
            Assert.NotNull(testDataList2);
            Assert.AreNotEqual(testDataList1, testDataList2);

            // Init
            ProductLineListViewModel testViewModel = new ProductLineListViewModel(new ObservableCollection<ProductLineModel>(testDataList1));

            // Attempt to refresh
            testViewModel.RefreshList(testDataList2);

            // Postconditions
            Assert.NotNull(testViewModel.ProductLineList);
            Assert.AreEqual(testDataList2, testViewModel.ProductLineList);
            Assert.AreNotSame(testDataList2, testViewModel.ProductLineList);
        }


        /// <summary>
        /// SelectedProductLine property must always return null to pass this test.
        /// 
        /// Or well, try some inputs and assume that there isn't some special if statement
        /// used to trick the tests.
        /// 
        /// This is required so that the view will reset it's SelectedItem property when
        /// the user returns to the page. (Assume 2-way binding)
        /// </summary>
        [Test, TestCaseSource("PRODUCT_LINE_LIST_TEST_DATA_01")]
        public void SelectedProductLineAlwaysReturnsNull_ProvidedInitList(List<ProductLineModel> testData)
        {
            // Preconditions
            // Dunno

            // Init
            ProductLineListViewModel testViewModel = new ProductLineListViewModel(new ObservableCollection<ProductLineModel>(testData));

            // Postconditions
            Assert.IsNull(testViewModel.SelectedProductLine);
        }

        /// <summary>
        /// See above - tho this time empty init list.
        /// </summary>
        [Test]
        public void SelectedProductLineAlwaysReturnsNull_EmptyInitList()
        {
            // Init
            ProductLineListViewModel testViewModel = new ProductLineListViewModel(new ObservableCollection<ProductLineModel>(new List<ProductLineModel>()));

            // Postconditions
            Assert.IsNull(testViewModel.SelectedProductLine);
        }


        [TearDown]
        public void Cleanup()
        {
            // Remove all settings configuration
            // aka. reset everything to default
            Settings.ClearEverything();
        }


    }
}
