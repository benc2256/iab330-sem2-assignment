﻿using Moq;
using NUnit.Framework;
using SupplyReview.Models;
using SupplyReview.Services;
using SupplyReview.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplyReview.Tests.UnitTests.ViewModels
{
    [TestFixture]
    class TestProductIndexViewModel
    {
        // Test Data //----

        /// <summary>
        /// Useful when needing to test a single list and filtering
        /// </summary>
        static object[] PRODUCT_TEST_DATA_02 =
        {
            new object [] // Case 1
            {
                new List<ProductModel>
                {
                    { new ProductModel() {_id="1", ProductLine="iPhone", Name="iPhone 3G", Category="Smartphone", Description="This could be copyright infringement?", ExtraInfo="Cost too much", Media="http://www.Idunno.com/image.jpg.png.stop"} },
                    { new ProductModel() {_id="2", ProductLine="Cotman", Name="Pan Colours 12pk", Category="Watercolour", Description="Oh, an actual art supply.", ExtraInfo="Writing Data is Hard", Media="http://www.wwwwhy.com/image.png.jpg"} }
                }
            },
            // Should have more cases
        };

        // Testing //----

        [SetUp]
        public void BeforeTestSuite()
        {
            // Do stuff here maybe? If needed.
        }


        /// <summary>
        /// For this test to pass, each product line in the test set is filtered
        /// on, and the viewmodel must correctly filter the test data.
        /// </summary>
        /// <param name="testData"></param>
        [Test, TestCaseSource("PRODUCT_TEST_DATA_02")]
        public void CanViewModelFilterOnProductLineCorrectly(List<ProductModel> testData)
        {
            // Preconditions
            Assert.IsNotEmpty(testData);

            // Arrange
            var mockServiceRep = new Mock<IProductService>();
            mockServiceRep.Setup(service => service.GetProducts()).Returns(testData.ToArray()); // should really be a list.

            var mockService = mockServiceRep.Object;
            var testViewModel = new ProductIndexViewModel(mockService);

            var productLines = testData.Select(product => product.ProductLine).Distinct().ToList();
            foreach (var productLineID in productLines)
            {
                // Act
                testViewModel.SetFilterOnProductLine(new ProductLineModel() { _id = productLineID });

                // Assert
                var expected = testData.Where(product => product.ProductLine == productLineID).ToList();
                Assert.AreEqual(expected, testViewModel.ListViewModel.ProductList, "Failure on productLine: " + productLineID);
            }

        }


        [TearDown]
        public void Cleanup()
        {
            // Remove all settings configuration
            // aka. reset everything to default
            Settings.ClearEverything();
        }
    }
}
