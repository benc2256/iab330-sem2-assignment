﻿using NUnit.Framework;
using System;
using SupplyReview.ViewModels;
using SupplyReview.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;


namespace SupplyReview.Tests.UnitTests.ViewModels
{
    [TestFixture]
    public class TestBrandListViewModel
    {
        static object[] BRAND_LIST_TEST_DATA_01 =
        {
            new object[] // case1
            {
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "2", TradingName = "def", LegalName = "DEF", Description = "This is brand DEF", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                }
            },
            new object[] // case2
            {
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "2", TradingName = "def", LegalName = "DEF", Description = "This is brand DEF", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                }
            }
        };
        static object[] BRAND_LIST_TEST_DATA_02 =
        {
            new object[] // case1
            {
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "2", TradingName = "def", LegalName = "DEF", Description = "This is brand DEF", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                },
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "3", TradingName = "ghi", LegalName = "GHI", Description = "This is brand GHI", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "4", TradingName = "jkl", LegalName = "JKL", Description = "This is brand JKL", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                }
            },
            new object[] // case2
            {
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "2", TradingName = "def", LegalName = "DEF", Description = "This is brand DEF", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                },
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "3", TradingName = "ghi", LegalName = "GHI", Description = "This is brand GHI", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "4", TradingName = "jkl", LegalName = "JKL", Description = "This is brand JKL", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                }
            },
        };

        [Test]
        public void CanInstantiateViewModel()
        {
            BrandListViewModel testViewModel = new BrandListViewModel(null);

            Assert.Pass("Your first passing test");
        }

        [Test, TestCaseSource("BRAND_LIST_TEST_DATA_01")]
        public void CanAssignListToViewModel(List<BrandModel> testData)
        {
            Assert.NotNull(testData);

            BrandListViewModel testViewModel = new BrandListViewModel(new ObservableCollection<BrandModel>(testData));

            Assert.NotNull(testViewModel.BrandList);
            Assert.AreEqual(testViewModel.BrandList, testData);
            Assert.AreNotSame(testData, testViewModel.BrandList);
        }

        [Test, TestCaseSource("BRAND_LIST_TEST_DATA_02")]
        public void CanRefreshList(List<BrandModel> testData1, List<BrandModel> testData2)
        {
            Assert.NotNull(testData1);
            Assert.NotNull(testData2);
            Assert.AreNotEqual(testData1, testData2);
            BrandListViewModel testViewModel = new BrandListViewModel(new ObservableCollection<BrandModel>(testData1));

            testViewModel.RefreshList(testData2);

            Assert.NotNull(testViewModel.BrandList);
            Assert.AreEqual(testViewModel.BrandList, testData2);
            Assert.AreNotSame(testViewModel.BrandList, testData2);
        }

        [Test, TestCaseSource("BRAND_LIST_TEST_DATA_01")]
        public void SelectedBrandAlwaysReturnsNull_ProvidedInitList(List<BrandModel> testData)
        {
            Assert.NotNull(testData);

            BrandListViewModel testViewModel = new BrandListViewModel(new ObservableCollection<BrandModel>(testData));

            Assert.IsNull(testViewModel.SelectedBrand);
        }

        [Test]
        public void SelectedBrandAlwaysReturnsNull_EmptyInitList()
        {
            BrandListViewModel testViewModel = new BrandListViewModel(new ObservableCollection<BrandModel>(new List<BrandModel>()));

            Assert.IsNull(testViewModel.SelectedBrand);
        }
    }
}
