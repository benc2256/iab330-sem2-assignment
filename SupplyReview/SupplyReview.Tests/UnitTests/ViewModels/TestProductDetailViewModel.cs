﻿using Moq;
using NUnit.Framework;
using SupplyReview.Models;
using SupplyReview.Services;
using SupplyReview.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplyReview.Tests.UnitTests.ViewModels
{
    [TestFixture]
    class TestProductDetailViewModel
    {
        // Test Data //----

        /// <summary>
        /// Good for... uh, testing this stuff...
        /// </summary>
        static object[] PRODUCT_DETAIL_TEST_DATA_01 =
        {
            new object[] // Case 1
            {

                new ProductModel() { _id="1", Category="1111", Description="It's the coco fruit!", ExtraInfo="Of the coco tree!", Media="", Name="Coconut", ProductLine="128" },
                new List<ProductLineModel>()
                {
                    new ProductLineModel() {_id="128", Brand="413", Description="The cocopalm family!", Media="", Name="Cocopalm"}
                },
                new List<BrandModel>()
                {
                    new BrandModel() {_id="413", Description="Someone has to do it!", EstablishmentDate= new DateTime(), LegalName="Farmer Co", Media="", TradingName="Famers" }
                },

                new List<CategoryModel>()
                {
                    new CategoryModel() {_id="1111", Description="Fancy", Media="http://OnlyTheFanciest.fancy", Name="Fancy Name"}
                },

                new List<ReviewModel>()
                {
                    { new ReviewModel(){_id = "1", Content = "Content for review 1", DateCreated = new DateTime(2000,1,1), DateModified = new DateTime(2000,1,1), Media = "http://www.look-at-me.com", Product = "1", Rating = 1, User = "1"} },
                    { new ReviewModel(){_id = "2", Content = "Content for review 2", DateCreated = new DateTime(2010,1,1), DateModified = new DateTime(2010,1,1), Media = "http://www.look-at-me.com", Product = "2", Rating = 2, User = "2"} }
                }
            },
            //new object[] // Case 2 - Too lazy to write a second case for now.
            //{

            //}
        };

        // Testing //----

        [SetUp]
        public void BeforeTestSuite()
        {
            // Do stuff here maybe? If needed.
        }


        /// <summary>
        /// Test to see if extra context that isn't in the model can be obtained. - Product Line Name
        /// </summary>
        /// <param name="detailSelected"></param>
        /// <param name="testProductLineDataSource"></param>
        /// <param name="testBrandDataSource"></param>
        /// <param name="testCategoryDataSource"></param>
        /// <param name="testReviewDataSource"></param>
        [Test, TestCaseSource("PRODUCT_DETAIL_TEST_DATA_01")]
        public void DoesMyModelPropertyGetExtraContextNotContainedInModel_ProductLineName(ProductModel detailSelected, List<ProductLineModel> testProductLineDataSource, List<BrandModel> testBrandDataSource, List<CategoryModel> testCategoryDataSource, List<ReviewModel> testReviewDataSource)
        {
            // Arrange
            var mockProductLineServiceRep = new Mock<IProductLineService>();
            mockProductLineServiceRep.SetupGet(service => service.ProductLines).Returns(testProductLineDataSource);

            var mockBrandServiceRep = new Mock<IBrandCatalogService>();
            mockBrandServiceRep.Setup(service => service.GetAllBrands()).Returns(testBrandDataSource);

            var mockReviewServiceRep = new Mock<IReviewService>();
            mockReviewServiceRep.Setup(service => service.GetReviewsByProduct(It.IsAny<string>())).Returns(testReviewDataSource);

            var mockCategoryServiceRep = new Mock<ICategoryService>();
            mockCategoryServiceRep.Setup(service => service.Categories).Returns(testCategoryDataSource);

            var mockProductLineService = mockProductLineServiceRep.Object;
            var mockBrandService = mockBrandServiceRep.Object;
            var mockCategoryService = mockCategoryServiceRep.Object;
            var mockReviewService = mockReviewServiceRep.Object;

            // Act

            // Init
            ProductDetailViewModel testViewModel = new ProductDetailViewModel(mockProductLineService, mockBrandService, mockCategoryService, mockReviewService);
            testViewModel.MyModel = detailSelected;

            // Assert
            Assert.AreEqual(testProductLineDataSource, mockProductLineService.ProductLines);
            Assert.AreEqual(mockProductLineService.ProductLines[0].Name, testViewModel.ProductLine_Name);
        }


        /// <summary>
        /// Test to see if extra context that isn't in the model can be obtained. - Brand Legal Name
        /// </summary>
        /// <param name="detailSelected"></param>
        /// <param name="testProductLineDataSource"></param>
        /// <param name="testBrandDataSource"></param>
        /// <param name="testCategoryDataSource"></param>
        /// <param name="testReviewDataSource"></param>
        [Test, TestCaseSource("PRODUCT_DETAIL_TEST_DATA_01")]
        public void DoesMyModelPropertyGetExtraContextNotContainedInModel_BrandName(ProductModel detailSelected, List<ProductLineModel> testProductLineDataSource, List<BrandModel> testBrandDataSource, List<CategoryModel> testCategoryDataSource, List<ReviewModel> testReviewDataSource)
        {
            // Arrange
            var mockProductLineServiceRep = new Mock<IProductLineService>();
            mockProductLineServiceRep.SetupGet(service => service.ProductLines).Returns(testProductLineDataSource);

            var mockBrandServiceRep = new Mock<IBrandCatalogService>();
            mockBrandServiceRep.Setup(service => service.GetAllBrands()).Returns(testBrandDataSource);

            var mockCategoryServiceRep = new Mock<ICategoryService>();
            mockCategoryServiceRep.Setup(service => service.Categories).Returns(testCategoryDataSource);

            var mockReviewServiceRep = new Mock<IReviewService>();
            mockReviewServiceRep.Setup(service => service.GetReviewsByProduct(It.IsAny<string>())).Returns(testReviewDataSource);

            var mockProductLineService = mockProductLineServiceRep.Object;
            var mockBrandService = mockBrandServiceRep.Object;
            var mockCategoryService = mockCategoryServiceRep.Object;
            var mockReviewService = mockReviewServiceRep.Object;

            // Act

            // Init
            ProductDetailViewModel testViewModel = new ProductDetailViewModel(mockProductLineService, mockBrandService, mockCategoryService, mockReviewService);
            testViewModel.MyModel = detailSelected;

            // Assert
            Assert.AreEqual(testBrandDataSource, mockBrandService.GetAllBrands());
            Assert.AreEqual(mockBrandService.GetAllBrands()[0].LegalName, testViewModel.Brand_Name);
        }

        /// <summary>
        /// Test to see if extra context that isn't in the model can be obtained. - CategoryName
        /// </summary>
        /// <param name="detailSelected"></param>
        /// <param name="testProductLineDataSource"></param>
        /// <param name="testBrandDataSource"></param>
        /// <param name="testCategoryDataSource"></param>
        /// <param name="testReviewDataSource"></param>
        [Test, TestCaseSource("PRODUCT_DETAIL_TEST_DATA_01")]
        public void DoesMyModelPropertyGetExtraContextNotContainedInModel_CategoryName(ProductModel detailSelected, List<ProductLineModel> testProductLineDataSource, List<BrandModel> testBrandDataSource, List<CategoryModel> testCategoryDataSource, List<ReviewModel> testReviewDataSource)
        {
            // Arrange
            var mockProductLineServiceRep = new Mock<IProductLineService>();
            mockProductLineServiceRep.SetupGet(service => service.ProductLines).Returns(testProductLineDataSource);

            var mockBrandServiceRep = new Mock<IBrandCatalogService>();
            mockBrandServiceRep.Setup(service => service.GetAllBrands()).Returns(testBrandDataSource);

            var mockCategoryServiceRep = new Mock<ICategoryService>();
            mockCategoryServiceRep.Setup(service => service.Categories).Returns(testCategoryDataSource);

            var mockReviewServiceRep = new Mock<IReviewService>();
            mockReviewServiceRep.Setup(service => service.GetReviewsByProduct(It.IsAny<string>())).Returns(testReviewDataSource);

            var mockProductLineService = mockProductLineServiceRep.Object;
            var mockBrandService = mockBrandServiceRep.Object;
            var mockCategoryService = mockCategoryServiceRep.Object;
            var mockReviewService = mockReviewServiceRep.Object;

            // Act

            // Init
            ProductDetailViewModel testViewModel = new ProductDetailViewModel(mockProductLineService, mockBrandService, mockCategoryService, mockReviewService);
            testViewModel.MyModel = detailSelected;

            // Assert
            Assert.AreEqual(testCategoryDataSource, mockCategoryService.Categories);
            Assert.AreEqual(mockCategoryService.Categories[0].Name, testViewModel.Category_Name);
        }



        /// <summary>
        /// Test that the review list can be instantiated.
        /// </summary>
        /// <param name="detailSelected"></param>
        /// <param name="testProductLineDataSource"></param>
        /// <param name="testBrandDataSource"></param>
        /// <param name="testCategoryDataSource"></param>
        /// <param name="testReviewDataSource"></param>
        [Test, TestCaseSource("PRODUCT_DETAIL_TEST_DATA_01")]
        public void TestReviewListInstantiation(ProductModel detailSelected, List<ProductLineModel> testProductLineDataSource, List<BrandModel> testBrandDataSource, List<CategoryModel> testCategoryDataSource, List<ReviewModel> testReviewDataSource)
        {
            Assert.NotNull(testReviewDataSource);
            // Arrange
            var mockProductLineServiceRep = new Mock<IProductLineService>();
            mockProductLineServiceRep.SetupGet(service => service.ProductLines).Returns(testProductLineDataSource);

            var mockBrandServiceRep = new Mock<IBrandCatalogService>();
            mockBrandServiceRep.Setup(service => service.GetAllBrands()).Returns(testBrandDataSource);

            var mockCategoryServiceRep = new Mock<ICategoryService>();
            mockCategoryServiceRep.Setup(service => service.Categories).Returns(testCategoryDataSource);

            var mockReviewServiceRep = new Mock<IReviewService>();
            mockReviewServiceRep.Setup(service => service.GetReviewsByProduct(It.IsAny<string>())).Returns(testReviewDataSource);

            var mockProductLineService = mockProductLineServiceRep.Object;
            var mockBrandService = mockBrandServiceRep.Object;

            var mockCategoryService = mockCategoryServiceRep.Object;
            var mockReviewService = mockReviewServiceRep.Object;

            
            ProductDetailViewModel testViewModel = new ProductDetailViewModel(mockProductLineService, mockBrandService, mockCategoryService, mockReviewService);
            testViewModel.MyModel = detailSelected;

            Assert.NotNull(testViewModel.ReviewListViewModel.ReviewList);
            Assert.AreEqual(testReviewDataSource, testViewModel.ReviewListViewModel.ReviewList);
        }

        [TearDown]
        public void Cleanup()
        {
            // Remove all settings configuration
            // aka. reset everything to default
            Settings.ClearEverything();
        }
    }
}
