﻿using NUnit.Framework;
using SupplyReview.Models;
using SupplyReview.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplyReview.Tests.UnitTests.ViewModels
{
    [TestFixture]
    public class TestReviewListViewModel
    {
        static object[] REVIEW_LIST_TEST_DATA_01 =
        {
            new object[]
            {
                new List<ReviewModel>
                {
                    { new ReviewModel(){_id = "1", Content = "Content for review 1", DateCreated = new DateTime(2000,1,1), DateModified = new DateTime(2000,1,1), Media = "http://www.look-at-me.com", Product = "1", Rating = 1, User = "1"} },
                    { new ReviewModel(){_id = "2", Content = "Content for review 2", DateCreated = new DateTime(2010,1,1), DateModified = new DateTime(2010,1,1), Media = "http://www.look-at-me.com", Product = "2", Rating = 2, User = "2"} }
                }
            },
            new object[]
            {
                new List<ReviewModel>
                {
                    { new ReviewModel(){_id = "2", Content = "Content for review 2", DateCreated = new DateTime(2010,1,1), DateModified = new DateTime(2010,1,1), Media = "http://www.look-at-me.com", Product = "2", Rating = 2, User = "2"} },
                    { new ReviewModel(){_id = "1", Content = "Content for review 1", DateCreated = new DateTime(2000,1,1), DateModified = new DateTime(2000,1,1), Media = "http://www.look-at-me.com", Product = "1", Rating = 1, User = "1"} }
                }
            }
        };

        static object[] REVIEW_LIST_TEST_DATA_02 =
{
            new object[]
            {
                new List<ReviewModel>
                {
                    { new ReviewModel(){_id = "1", Content = "Content for review 1 List 1", DateCreated = new DateTime(2000,1,1), DateModified = new DateTime(2000,1,1), Media = "http://www.look-at-me.com", Product = "1", Rating = 1, User = "1"} },
                    { new ReviewModel(){_id = "2", Content = "Content for review 2 List 1", DateCreated = new DateTime(2010,1,1), DateModified = new DateTime(2010,1,1), Media = "http://www.look-at-me.com", Product = "2", Rating = 2, User = "2"} }
                },
                new List<ReviewModel>
                {
                    { new ReviewModel(){_id = "1", Content = "Content for review 1 List 2", DateCreated = new DateTime(2000,1,1), DateModified = new DateTime(2000,1,1), Media = "http://www.look-at-me.com", Product = "1", Rating = 1, User = "1"} },
                    { new ReviewModel(){_id = "2", Content = "Content for review 2 List 2", DateCreated = new DateTime(2010,1,1), DateModified = new DateTime(2010,1,1), Media = "http://www.look-at-me.com", Product = "2", Rating = 2, User = "2"} }
                }
            },
            new object[]
            {
                new List<ReviewModel>
                {
                    { new ReviewModel(){_id = "2", Content = "Content for review 2 List 1", DateCreated = new DateTime(2010,1,1), DateModified = new DateTime(2010,1,1), Media = "http://www.look-at-me.com", Product = "2", Rating = 2, User = "2"} },
                    { new ReviewModel(){_id = "1", Content = "Content for review 1 List 1", DateCreated = new DateTime(2000,1,1), DateModified = new DateTime(2000,1,1), Media = "http://www.look-at-me.com", Product = "1", Rating = 1, User = "1"} }
                },
                new List<ReviewModel>
                {
                    { new ReviewModel(){_id = "2", Content = "Content for review 2 List 2", DateCreated = new DateTime(2010,1,1), DateModified = new DateTime(2010,1,1), Media = "http://www.look-at-me.com", Product = "2", Rating = 2, User = "2"} },
                    { new ReviewModel(){_id = "1", Content = "Content for review 1 List 2", DateCreated = new DateTime(2000,1,1), DateModified = new DateTime(2000,1,1), Media = "http://www.look-at-me.com", Product = "1", Rating = 1, User = "1"} }
                }
            }
        };

        [Test]
        public void CanInstantiateViewModel()
        {
            ReviewListViewModel testViewModel = new ReviewListViewModel(null);

            Assert.Pass("Your first passing test");
        }


        [Test, TestCaseSource("REVIEW_LIST_TEST_DATA_01")]
        public void CanAssignAListToViewModel(List<ReviewModel> testData)
        {
            Assert.NotNull(testData);

            ReviewListViewModel testViewModel = new ReviewListViewModel(new ObservableCollection<ReviewModel>(testData));

            Assert.NotNull(testViewModel.ReviewList);
            Assert.AreEqual(testData, testViewModel.ReviewList);
            Assert.AreNotSame(testData, testViewModel.ReviewList);
        }

        [Test, TestCaseSource("REVIEW_LIST_TEST_DATA_02")]
        public void CanRefreshListOfViewModel(List<ReviewModel> testDataList1, List<ReviewModel> testDataList2)
        {
            Assert.NotNull(testDataList1);
            Assert.NotNull(testDataList2);
            Assert.AreNotEqual(testDataList1, testDataList2);

            ReviewListViewModel testViewModel = new ReviewListViewModel(new ObservableCollection<ReviewModel>(testDataList1));

            testViewModel.Refresh(testDataList2);

            Assert.NotNull(testViewModel.ReviewList);
            Assert.AreEqual(testDataList2, testViewModel.ReviewList);
            Assert.AreNotSame(testDataList2, testViewModel.ReviewList);
        }
    }
}
