﻿using Moq;
using NUnit.Framework;
using SupplyReview.Models;
using SupplyReview.Services;
using SupplyReview.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace SupplyReview.Tests.UnitTests.ViewModels
{
    [TestFixture]
    public class TestBrandIndexViewModel
    {
        static object[] BRAND_LIST_TEST_DATA_03 =
        {
            new object[] // case1
            {
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "2", TradingName = "def", LegalName = "DEF", Description = "This is brand DEF", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                }
            },
            new object[] // case2
            {
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "2", TradingName = "def", LegalName = "DEF", Description = "This is brand DEF", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "2", TradingName = "ghi", LegalName = "GHI", Description = "This is brand GHI", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                }
            }
        };

        static object[] BRAND_LIST_TEST_DATA_04 =
        {
            new object[] // case1
            {
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "2", TradingName = "def", LegalName = "DEF", Description = "This is brand DEF", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                },
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} }
                }
            },
            new object[] // case2
            {
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} },
                    {new BrandModel(){_id = "2", TradingName = "def", LegalName = "DEF", Description = "This is brand DEF", EstablishmentDate = new DateTime(2010,1,1), Media = "http://www.look-at-me.com"} }
                },
                new List<BrandModel>
                {
                    {new BrandModel(){_id = "1", TradingName = "abc", LegalName = "ABC", Description = "This is brand ABC", EstablishmentDate = new DateTime(2000,1,1), Media = "http://www.look-at-me.com"} }
                }
            }
        };


        [Test]
        public void CanInstantiateViewModelWithEmptyMock()
        {
            var mockServiceRep = new Mock<IBrandCatalogService>();

            mockServiceRep.Setup(service => service.GetAllBrands()).Returns(new List<BrandModel>());

            var mockService = mockServiceRep.Object;

            var testViewModel = new BrandIndexViewModel(mockService);

            Assert.AreEqual(mockService.GetAllBrands(), testViewModel.ListViewModel.BrandList);
            Assert.IsEmpty(mockService.GetAllBrands());
            Assert.IsEmpty(testViewModel.ListViewModel.BrandList);
        }

        [Test, TestCaseSource("BRAND_LIST_TEST_DATA_03")]
        public void CanInstantiateViewModelWithPopulatedMock(List<BrandModel> testData)
        {
            var mockServiceRep = new Mock<IBrandCatalogService>();
            mockServiceRep.Setup(service => service.GetAllBrands()).Returns(testData);

            var mockService = mockServiceRep.Object;

            BrandIndexViewModel testViewModel = new BrandIndexViewModel(mockService);

            Assert.AreEqual(testData, mockService.GetAllBrands());
            Assert.AreEqual(testData, testViewModel.ListViewModel.BrandList);
        }
    }
}
