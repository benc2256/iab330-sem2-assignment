﻿using NUnit.Framework;
using SupplyReview.Models;
using SupplyReview.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplyReview.Tests.UnitTests
{
    [TestFixture]
    public class TestProductLineService
    {
        [SetUp]
        public void BeforeTestSuite()
        {
            // Settings.UrlBase = "http://localhost:3000";
           // Settings.UrlBase = "http://40.121.49.6";

        }


        [Test]
        public void CanInstantiateService()
        {
            ProductLineService testService = new ProductLineService("");

            Assert.Pass("Your first passing test");
        }


        [Test]
        public void CanGetAThing()
        {
            ProductLineService productLineService = new ProductLineService("");

            //var result = //await productLineService.GetAllProductLines();

            if (productLineService.ProductLines.Any()) // Need to have something in the db for this to pass
            {
                Assert.Pass("Got something!: " + productLineService);
            }
            Assert.Fail("Failed to get anything from the database");
        }


        [TearDown]
        public void Cleanup()
        {
            // Remove all settings configuration
            // aka. reset everything to default
            Settings.ClearEverything();
        }
    }
}
