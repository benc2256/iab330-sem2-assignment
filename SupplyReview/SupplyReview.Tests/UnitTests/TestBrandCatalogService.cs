﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplyReview.Models;
using SupplyReview.Services;

namespace SupplyReview.Tests.UnitTests
{
    [TestFixture]
    public class TestBrandCatalogService
    {
        [TestFixtureSetUp]
        public void BeforeTestSuite()
        {
            //Settings.UrlBase = "http://localhost:3000";
            Settings.UrlBase = "http://40.121.49.6:27017";
        }

        [Test]
        public void GetProductLines()
        {
            String brandID = "Something";
            BrandCatalogService brandService = new BrandCatalogService("");
            List<ProductLineModel> productLinesActual = new List<ProductLineModel>();
            List<ProductLineModel> productLinesExpected = new List<ProductLineModel>();

            productLinesActual = brandService.GetProductLinesByBrandID(brandID);

            //Add something to the expected stuff
            // Assert.Equals(productLinesActual, productLinesExpected);
            Assert.Fail("No test data");
        }


        [Test]
        public void GetProducts()
        {
            String brandID = "Something";
            BrandCatalogService brandService = new BrandCatalogService("");
            List<ProductModel> productsActual = new List<ProductModel>();
            List<ProductModel> productsExpected = new List<ProductModel>();

            productsActual = brandService.GetProductsByBrandID(brandID);

            //Add something to the expected stuff
            // Assert.Equals(productsActual, productsExpected);
            Assert.Fail("No test data");
        }
    }
}
