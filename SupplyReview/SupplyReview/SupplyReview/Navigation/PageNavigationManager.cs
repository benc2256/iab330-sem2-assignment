﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using SupplyReview.Services;

namespace SupplyReview.Navigation
{
    /// <summary>
    /// Singleton used to manage navigation
    /// </summary>
    public class PageNavigationManager
    {
        private static PageNavigationManager instance;
        private INavigation navigation;

        private PageNavigationManager() { }

        public static PageNavigationManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PageNavigationManager();
                }
                return instance;
            }
        }

        public INavigation Navigation
        {
            set { navigation = value; }
        }


        // Methods for page switching //----

        // ProductLineIndexPage //

        /// <summary>
        /// Show ProductLineIndexPage with all results/no filters
        /// </summary>
        public void ShowProductLineIndexPage()
        {
            var view = new Views.ProductLineIndexPage();
            var viewModel = new ViewModels.ProductLineIndexViewModel(new Services.ProductLineService(""));
            view.BindingContext = viewModel;
            navigation.PushAsync(view);
        }


        // ProductListIndexPage //

        /// <summary>
        /// Show ProductListIndexPage with a filter on provided product line.
        /// </summary>
        /// <param name="productLine">The product line to filter on.</param>
        public void ShowProductListIndexPageFilterProductLine(Models.ProductLineModel productLine)
        {
            // Init
            var view = new Views.ProductListIndexPage();
            var viewModel = new ViewModels.ProductIndexViewModel(new Services.ProductService(""));
            viewModel.SetFilterOnProductLine(productLine);
            viewModel.PageTitle = "Products - " + productLine.Name;
            // Set view binding context
            view.BindingContext = viewModel;
            // Push view to navigation
            navigation.PushAsync(view);
        }


        /// <summary>
        /// Show ProductListIndexPage with all results/no filters
        /// </summary>
        public void ShowProductListIndexPage()
        {
            // Init
            var view = new Views.ProductListIndexPage();
            var viewModel = new ViewModels.ProductIndexViewModel(new Services.ProductService(""));
            viewModel.PageTitle = "Products";
            // Set view binding context
            view.BindingContext = viewModel;
            // Push view to navigation
            navigation.PushAsync(view);
        }

        
        // ProductDetailPage //

        /// <summary>
        /// Show ProductDetailPage for specified product.
        /// </summary>
        /// <param name="product">The product to show the detail page for.</param>
        public void ShowProductDetailPage(Models.ProductModel product)
        {
            // Init
            var view = new Views.ProductDetailPage();
            var viewModel = new ViewModels.ProductDetailViewModel(new Services.ProductLineService(""), new Services.BrandCatalogService(""), new Services.CategoryService(""), new Services.ReviewService(""));

            //Configure to product to be displayed
            viewModel.MyModel = product;

            // Set view binding context
            view.BindingContext = viewModel;
            // Push view to navigation
            navigation.PushAsync(view);
        }

        /// <summary>
        /// Displays the brand catalog page
        /// </summary>
        /// <param name="brand">The brand being displayed</param>
        public void ShowBrandCatalogPage(Models.BrandModel brand)
        {
            //Initialise the view and view model
            var view = new Views.BrandCatalogPage();
            var viewModel = new ViewModels.BrandCatalogViewModel(brand, new BrandCatalogService(""));
            //Set the binding context of the page to the view model
            view.BindingContext = viewModel;
            navigation.PushAsync(view);
        }

        /// <summary>
        /// Shows the brand index page
        /// </summary>
        public void ShowBrandIndexPage()
        {
            //Initialise the view and view model
            var view = new Views.BrandIndexPage();
            var viewModel = new ViewModels.BrandIndexViewModel(new BrandCatalogService(""));
            //Set the binding context of the view to the view model
            view.BindingContext = viewModel;
            navigation.PushAsync(view);
        }

        public void ShowHomePage()
        {
            //var view = new Views.HomePage();
            //navigation.PushAsync(view);

            // This nifty little built in function takes yah back to the root.
            navigation.PopToRootAsync();

        }





    }
}
