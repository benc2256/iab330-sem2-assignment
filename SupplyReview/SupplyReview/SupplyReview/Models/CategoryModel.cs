﻿using System;

namespace SupplyReview.Models
{
    public class CategoryModel : APIModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Media { get; set; }

        public CategoryModel() : base()
        {
        }
    }
}
