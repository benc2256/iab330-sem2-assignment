﻿using System;

namespace SupplyReview.Models
{
    public class ProductLineModel : APIModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Media { get; set; }

        //public BrandModel Brand { get; set; }
        /// <summary>
        /// Obj ID to reference brand
        /// </summary>
        public string Brand { get; set; }

        public ProductLineModel() : base()
        {
        }
    }
}
