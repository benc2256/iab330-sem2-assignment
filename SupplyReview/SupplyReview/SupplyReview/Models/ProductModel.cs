﻿using System;

namespace SupplyReview.Models
{
    public class ProductModel : APIModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// Obj ID to reference category
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// Obj ID to reference product line
        /// </summary>
        public string ProductLine { get; set; }
        public string ExtraInfo { get; set; }
        public string Media { get; set; }

        public ProductModel() : base()
        {
        }
    }
}
