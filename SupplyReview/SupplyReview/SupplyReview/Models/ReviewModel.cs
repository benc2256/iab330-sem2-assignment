﻿using System;
using Newtonsoft.Json;

namespace SupplyReview.Models
{
    public class ReviewModel : APIModel
    {
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int Rating { get; set; }
        public string Content { get; set; }
        public string Media { get; set; }
        public string User { get; set; }
        public string Product { get; set; }

        public ReviewModel() : base()
        {
        }
    }
}
