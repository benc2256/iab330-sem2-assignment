﻿using System;
namespace SupplyReview.Models
{
    public class BrandModel : APIModel
    {
        public string LegalName { get; set; }
        public string TradingName { get; set; }
        public string Description { get; set; }
        public DateTime EstablishmentDate { get; set; }
        public string Media { get; set; }

        public BrandModel() : base()
        {
        }
    }
}
