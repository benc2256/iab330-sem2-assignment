﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SupplyReview.Services;
using SupplyReview.Models;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SupplyReview
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var viewModel = new ViewModels.HomePageViewModel();
            var view = new Views.HomePage() { BindingContext=viewModel };

            this.MainPage = new NavigationPage(view);

            // Pass navigation instance to navigation manager 
            Navigation.PageNavigationManager.Instance.Navigation = MainPage.Navigation;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
