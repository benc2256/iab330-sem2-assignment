﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SupplyReview.Extensions.Xaml
{
    [ContentProperty(nameof(ItemSource))]
    public class TabViewControl : Xam.Plugin.TabView.TabViewControl
    {
        public TabViewControl() : base(new List<Xam.Plugin.TabView.TabItem>(), 0)
        {

        }
    }
    [ContentProperty(nameof(Content))]
    public class TabItem : Xam.Plugin.TabView.TabItem
    {
        public TabItem() : base("", new ContentView())
        {

        }
    }

    //public class cry: Xam.Plugin.TabView.TabViewControl
}
