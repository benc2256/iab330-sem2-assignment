﻿using System.Collections.Generic;
using SupplyReview.Models;

namespace SupplyReview.Services
{
    public interface IReviewService
    {
        List<ReviewModel> GetReviewsByProduct(string productID);
        ReviewModel CreateReview(object review);
    }
}