
﻿using Newtonsoft.Json;
using SupplyReview.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplyReview.Services
{
    /// <summary>
    /// This class implments the connection to the API to access information related to brands
    /// </summary>
    public class BrandCatalogService : APIService, IBrandCatalogService
    {
        /// <summary>
        /// Implments the constructor
        /// </summary>
        public BrandCatalogService(string sessionToken) : base(sessionToken)
        {

        }

        /// <summary>
        /// Finds all brands in the datastore
        /// </summary>
        /// <returns>Returns a list of all brands</returns>
        public List<BrandModel> GetAllBrands()
        {
            List<BrandModel> brands;
            brands = JsonConvert.DeserializeObject<BrandModel[]>(this.sendRequest("GET", "/api/brand")).ToList();
            return brands;
        }

        /// <summary>
        /// Finds all product-lines that are linked with a specified brand
        /// </summary>
        /// <param name="brandID">The id of the brand being filtered</param>
        /// <returns>A List of all product-lines for the brand</returns>
        public List<ProductLineModel> GetProductLinesByBrandID(string brandID)
        {
            List<ProductLineModel> results = new List<ProductLineModel>();
            results = JsonConvert.DeserializeObject<List<ProductLineModel>>(this.sendRequest("GET", "/api/product-line/brand/" + brandID));
 
            return results;
        }

        /// <summary>
        /// Finds all products associated with a brand
        /// </summary>
        /// <param name="brandID">The brand being searched</param>
        /// <returns>A list of products associated with the brand</returns>
        public List<ProductModel> GetProductsByBrandID(string brandID)
        {
            List<ProductModel> results = new List<ProductModel>();
            results = JsonConvert.DeserializeObject<List<ProductModel>>(this.sendRequest("GET", "/api/product/brand/" + brandID));

            return results;
        }
    }
}