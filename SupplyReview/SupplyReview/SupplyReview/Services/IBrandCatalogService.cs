﻿using System.Collections.Generic;
using SupplyReview.Models;

namespace SupplyReview.Services
{
    public interface IBrandCatalogService
    {
        List<BrandModel> GetAllBrands();
        List<ProductLineModel> GetProductLinesByBrandID(string brandID);
        List<ProductModel> GetProductsByBrandID(string brandID);
    }
}