﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SupplyReview.Models;

namespace SupplyReview.Services
{
    public interface IProductLineService
    {
        IList<ProductLineModel> ProductLines { get; }

        Task<List<ProductLineModel>> GetPaginatedProductLines(int pageNumber);
        Task<List<ProductLineModel>> GetPaginatedProductLines(int pageNumber, int entriesPerPage);
        Task<ProductLineModel> GetProductLineById(string id);
        Task<List<ProductLineModel>> GetProductLineRange(int startIndex, int count);
        Task<List<ProductLineModel>> GetProductLinesById(List<string> ids);
    }
}