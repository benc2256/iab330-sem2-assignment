﻿using System.Collections.Generic;
using SupplyReview.Models;

namespace SupplyReview.Services
{
    public interface ICategoryService
    {
        IList<CategoryModel> Categories { get; }
    }
}