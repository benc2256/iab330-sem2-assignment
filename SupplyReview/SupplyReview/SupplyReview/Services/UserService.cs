﻿using System;
using SupplyReview.Models;
using Newtonsoft.Json;

namespace SupplyReview.Services
{
    public class UserService : APIService
    {

        public UserService(string sessionToken) : base(sessionToken)
        {
        }

        public string getUserMe()
        {
            return this.sendRequest("GET", "/api/user");
        }

        public UserModel getUserById(string id)
        {
            return JsonConvert.DeserializeObject<UserModel>(this.sendRequest("GET", "/api/user/" + id));
        }

    }
}
