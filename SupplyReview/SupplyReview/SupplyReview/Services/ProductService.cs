﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;
using SupplyReview.Models;

namespace SupplyReview.Services
{
    public class ProductService : APIService, IProductService
    {
        public ProductService(string sessionToken) : base(sessionToken)
        {
        }

        /// <summary>
        /// Gets a list of products
        /// </summary>
        /// <returns>Array of all products</returns>
        public ProductModel[] GetProducts()
        {
            try
            {
                return JsonConvert.DeserializeObject<ProductModel[]>(this.sendRequest("GET", "/api/product"));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }

        }
    }
}
