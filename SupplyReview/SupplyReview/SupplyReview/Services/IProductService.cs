﻿using SupplyReview.Models;

namespace SupplyReview.Services
{
    public interface IProductService
    {
        ProductModel[] GetProducts();
    }
}