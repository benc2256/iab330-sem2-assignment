﻿using Newtonsoft.Json;
using SupplyReview.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SupplyReview.Services
{
    /// <summary>
    /// This class implements the connection to the api to find information for reviews
    /// </summary>
    public class ReviewService : APIService, IReviewService
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sessionToken">session token for the service</param>
        public ReviewService(string sessionToken) : base(sessionToken)
        {
            // No additional construction behaviour - yet
        }

        /// <summary>
        /// Finds all products for a specified product
        /// </summary>
        /// <param name="productID">The id for the product being searched</param>
        /// <returns>A list of all reviews for the product</returns>
        public List<ReviewModel> GetReviewsByProduct(string productID)
        {
            List<ReviewModel> results = new List<ReviewModel>();
            results = JsonConvert.DeserializeObject<List<ReviewModel>>(this.sendRequest("GET", "/api/review/product/" + productID));
            return results;
        }

        public ReviewModel CreateReview(object review) {
            string response = this.sendRequest("POST", "/api/review", review);
            return String.IsNullOrEmpty(response) ? null : JsonConvert.DeserializeObject<ReviewModel>(response);
        }
    }
}
