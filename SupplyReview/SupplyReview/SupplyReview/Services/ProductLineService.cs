﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using SupplyReview.Models;
using Newtonsoft.Json;

namespace SupplyReview.Services
{
    public class ProductLineService : APIService, IProductLineService
    {
        // Constants //

        /// <summary>
        /// Amount of product line entries that is requested from the server per page
        /// </summary>
        static readonly int PRODUCT_LINE_PAGE_LENGTH = Settings.ProductLinesPerPage;


        // Constructor //

        public ProductLineService(string sessionToken) : base(sessionToken)
        {
            // No additional construction behaviour - yet
        }


        // Properties //
        // Connect to database collection

        private List<ProductLineModel> _productLines;
        /// <summary>
        /// The product line - Lazy loaded (Might also want to add a way to refresh product line)
        /// </summary>
        public IList<ProductLineModel> ProductLines {
            get {
                if (_productLines == null)
                {
                    _productLines = JsonConvert.DeserializeObject<ProductLineModel[]>(this.sendRequest("GET", "api/product-line")).ToList();
                }
                return _productLines;
            }
        }


        // Public Methods //

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //public async Task<List<ProductLineModel>> GetAllProductLines()
        //{
        //    try
        //    {
        //        var allProductLines = await ProductLines
        //            .Find(new BsonDocument())
        //            .ToListAsync();

        //        return allProductLines;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //    return null;
        //}


        /// <summary>
        /// Get multiple product lines by id
        /// </summary>
        /// <param name="ids">The ids of the product lines to get</param>
        /// <returns>List populated with values for each id given or null if none found.</returns>
        public async Task<List<ProductLineModel>> GetProductLinesById(List<string> ids)
        {
            // Error checking
            if (ids == null || ids.Count == 0)
            {
                throw new ArgumentException("id list cannot be null or have length 0: " + ids);
            }

            // Get the values
            List<ProductLineModel> values;
            try
            {
                values = await Task.Run<List<ProductLineModel>>( () => { return ProductLines.Where(productLine => ids.Contains(productLine._id)).ToList<ProductLineModel>(); });
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null; // log the error and return null
            }


            // Create response
            List<ProductLineModel> result = new List<ProductLineModel>();
            foreach (string id in ids)
            {
                result.Add(values.Where(productLine => productLine._id == id).Single());
            }

            return result;
        }


        /// <summary>
        /// Get a specific product line by id
        /// </summary>
        /// <param name="id">The id of the product to get.</param>
        /// <returns>The product with the given id or null if not found.</returns>
        public async Task<ProductLineModel> GetProductLineById(string id)
        {
            try
            {
                var result = await GetProductLinesById(new List<string> { id });
                return result.SingleOrDefault(null);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null; // Log the error and return null
            }

        }


        /// <summary>
        /// Get a range of product lines by index
        /// </summary>
        /// <param name="startIndex">The list index to start at.</param>
        /// <param name="count">The count of objects to get</param>
        /// <returns>A list of product lines that contains the requested range or null if not found</returns>
        public async Task<List<ProductLineModel>> GetProductLineRange(int startIndex, int count)
        {
            try
            {
                return await Task<List<ProductLineModel>>.Run( () => ProductLines.Skip(startIndex).Take(count).ToList());
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null; // Log the error and return null
            }
        }


        /// <summary>
        /// Get a paginated list of product lines
        /// </summary>
        /// <param name="pageNumber">The page number to be requested</param>
        /// <param name="entriesPerPage">The amount of entries per page</param>
        /// <returns>A list containing a paginated view of product lines</returns>
        public async Task<List<ProductLineModel>> GetPaginatedProductLines(int pageNumber, int entriesPerPage)
        {
            // Calc the start index
            int start = (pageNumber - 1) * entriesPerPage; // indexed from 0

            // Get the product lines for the given page
            return await GetProductLineRange(start, entriesPerPage);
        }


        /// <summary>
        /// Get the page using the default/user set pagination cut off.
        /// </summary>
        /// <param name="pageNumber">The page number to be requested</param>
        /// <returns>A list containing a paginated view of product lines</returns>
        public async Task<List<ProductLineModel>> GetPaginatedProductLines(int pageNumber)
        {
            return await GetPaginatedProductLines(pageNumber, PRODUCT_LINE_PAGE_LENGTH);
        }


        // Private Methods //

        /// <summary>
        /// Checks to see if a product line (via id) exists. 
        /// </summary>
        /// <param name="id">The id of the product to check</param>
        /// <returns>True if product line exists, false otherwise.</returns>
        private async Task<bool> DoesProductLineIdExist(string id)
        {
            if (await GetProductLineById(id) != null)
            {
                return true;
            }
            return false;
        }
    }
}
