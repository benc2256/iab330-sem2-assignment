﻿using Newtonsoft.Json;
using SupplyReview.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SupplyReview.Services
{
    public class CategoryService : APIService, ICategoryService
    {

        // Properties //
        // Connect to database collection

        private List<CategoryModel> _categories;
        /// <summary>
        /// The categories - Lazy loaded
        /// </summary>
        public IList<CategoryModel> Categories {
            get {
                if (_categories == null)
                {
                    _categories = JsonConvert.DeserializeObject<CategoryModel[]>(this.sendRequest("GET", "api/category")).ToList();
                }
                return _categories;
            }
        }


        public CategoryService(string sessionToken) : base(sessionToken)
        {
            // Does nothing.
        }


    }
}
