﻿using System;
using System.Net;
using Newtonsoft.Json;

using SupplyReview.Models;

namespace SupplyReview.Services
{
    public class APIService
    {
        private string sessionToken;

        public APIService(string sessionToken)
        {
            this.sessionToken = sessionToken;
        }

        public string sendRequest(string method, string endpoint, object data = null)
        {
            string url = String.Format("{0}/{1}", Settings.UrlBase, endpoint);
            WebClient client = new WebClient();
            client.Headers.Add("Authorization", sessionToken);

            switch (method.ToLower()) {

                case "get": return client.DownloadString(url);

                case "post":
                    try
                    {
                        string body = JsonConvert.SerializeObject(data);
                        client.Headers[HttpRequestHeader.ContentType] = "application/json";

                        return client.UploadString(url, "POST", body);
                    } catch (WebException ex) {
                        Console.WriteLine(ex.ToString());
                        return null;
                    }

                default: return null;

            }
        }
    }
}
