﻿using System;
using System.Collections.Generic;

using SupplyReview.Models;

namespace SupplyReview.Core
{
    public class Search
    {
        public Search()
        {
        }

        public static ProductModel[] GetProductsByName(ProductModel[] products, string query) {
            List<ProductModel> results = new List<ProductModel>();
            for (int i = 0; i < products.Length; i++)
            {
                string itemName = products[i].Name.ToLower().Trim();
                if (itemName.Contains(query))
                    results.Add(products[i]);
            }
            return results.ToArray();
        }
    }
}
