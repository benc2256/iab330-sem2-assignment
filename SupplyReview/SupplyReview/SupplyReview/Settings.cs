﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace SupplyReview
{
    public static class Settings
    {

        private static ISettings AppSettings {
            get {
                return CrossSettings.Current;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public static void ClearEverything()
        {
            AppSettings.Clear();
        }


        /// <summary>
        /// 
        /// </summary>
        public static string UrlBase {
            get => AppSettings.GetValueOrDefault(nameof(UrlBase), "http://40.121.49.6");
            set => AppSettings.AddOrUpdateValue(nameof(UrlBase), value);
        }
        public static void RemoveUrlBase() => AppSettings.Remove(nameof(UrlBase));


        /// <summary>
        /// 
        /// </summary>
        public static int ProductLinesPerPage {
            get => AppSettings.GetValueOrDefault(nameof(ProductLinesPerPage), 10);
            set => AppSettings.AddOrUpdateValue(nameof(ProductLinesPerPage), value);
        }
        public static void RemoveProductLinesPerPage() => AppSettings.Remove(nameof(ProductLinesPerPage));

    }


}

