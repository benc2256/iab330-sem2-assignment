﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using SupplyReview.Models;
using SupplyReview.Services;

namespace SupplyReview.ViewModels
{
    /// <summary>
    /// This class represents the view model for the "list" of product lines
    /// so you should be able to reuse this view model in many pages.
    /// </summary>
    public class ProductLineListViewModel : INotifyPropertyChanged
    {
        // Public Properties //----

        /// <summary>
        /// The list of product lines to be accessed by the view.
        /// </summary>
        public ObservableCollection<ProductLineModel> ProductLineList { get; set; }


        /// <summary>
        /// SelectedProductLine - bound to the SelectedItem property,
        /// allows for changes to be intercepted by the setter.
        /// 2 way binding resets the "selected item" value of the control
        /// to null. (The binding mode is set in the xaml)
        /// </summary>
        public ProductLineModel SelectedProductLine {
            get {
                return null;
            }
            set {
                if (value != null)
                {
                    // Bah, if it works - who cares if it's not called asynchronously?
                    // Actually, I dunno, maybe this is 100% fine (Man, MVVM and Async - together - is hard >_<)
                    Navigation.PageNavigationManager.Instance.ShowProductListIndexPageFilterProductLine(value);
                    // The below is required for 2-way binding I beleive
                    // Apparently it's a bug where the UI won't reset the item selected
                    // unless 2 way binding is used... so yeahhh
                    OnPropertyChanged(nameof(SelectedProductLine));
                }
            }
        }


        // Constructor //----

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="productLineList">The product line list to initially display.</param>
        public ProductLineListViewModel(ObservableCollection<ProductLineModel> productLineList)
        {
            this.ProductLineList = productLineList;
        }


        // Public Methods //----

        /// <summary>
        /// Updates the list of items to be displayed to the provided list.
        /// </summary>
        /// <param name="productLineList">The list to update to.</param>
        public void RefreshList(List<ProductLineModel> productLineList)
        {
            this.ProductLineList.Clear();
            foreach (ProductLineModel productLine in productLineList) this.ProductLineList.Add(productLine);
        }


        // Utility/Other //----

        // Something about the INotifyPropertyChanged interface? Dunno
        public event PropertyChangedEventHandler PropertyChanged;


        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
