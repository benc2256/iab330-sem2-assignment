﻿using SupplyReview.Models;
using SupplyReview.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using Xamarin.Forms;
using System.Windows.Input;

namespace SupplyReview.ViewModels
{

    /// <summary>
    /// This view model is used for the whole product detail page
    /// </summary>
    public class ProductDetailViewModel : INotifyPropertyChanged
    {
        public string StarRating { get; set; }

        private string _ReviewContent;
        public string ReviewContent
        {
            get
            {
                return this._ReviewContent ?? "";
            }
            set
            {
                this._ReviewContent = value;
            }
        }

        private ProductModel _myModel;
        /// <summary>
        /// The current model for the detail page to source data from.
        /// </summary>
        public ProductModel MyModel
        {
            get => _myModel;
            set
            {
                _myModel = value;
                InitExtraContext();
                OnPropertyChanged(nameof(MyModel));
            }
        }


        /// <summary>
        /// Extra context to know the product line name of the current myModel
        /// </summary>
        public string ProductLine_Name { get; set; }

        public ReviewListViewModel ReviewListViewModel { get; set; }


        /// <summary>
        /// Extra context to know the brand name of the current myModel
        /// </summary>
        public string Brand_Name { get; set; }


        /// <summary>
        /// Extra context to know the category name of the current myModel
        /// </summary>
        public string Category_Name { get; set; }

        public ICommand AddReviewCommand { get; set; }

        // Private Variables //
        private IProductLineService productLineService;
        private IBrandCatalogService brandCatalogService;
        private IReviewService reviewService;
        private ICategoryService categoryService;

        // Constructor //----

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="productLineService">Dependency to be injected</param>
        /// <param name="brandCatalogService">Dependency to be injected</param>
        /// <param name="categoryService">Dependency to be injected</param>
        /// <param name="reviewService">Dependency to be injected</param>
        public ProductDetailViewModel(IProductLineService productLineService, IBrandCatalogService brandCatalogService, ICategoryService categoryService, IReviewService reviewService)
        {
            // Dependency Injection
            this.productLineService = productLineService;
            this.brandCatalogService = brandCatalogService;
            this.categoryService = categoryService;
            this.reviewService = reviewService;
            ReviewListViewModel = new ReviewListViewModel(new ObservableCollection<ReviewModel>(new List<ReviewModel>()));

            this.ReviewContent = "";
            this.AddReviewCommand = new Command(AddReview);
        }


        // Private Methods //----

        private void AddReview()
        {
            if (String.IsNullOrEmpty(this.ReviewContent) || 
                String.IsNullOrEmpty(this.StarRating) ||
                !int.TryParse(this.StarRating.Substring(0, 1), out int stars))
            {
                Console.WriteLine("Invalid star rating given. {0}", this.StarRating ?? "(null)");
                return;
            }

            string today = String.Format("{0}-{1:00}-{2:00}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            bool result = this.reviewService.CreateReview(new
            {
                content = this.ReviewContent,
                dateCreated = today,
                dateModified = today,
                product = this.MyModel._id,
                rating = stars.ToString()
            }) != null;
            if (result) this.PopulateReviews();

            this.ReviewContent = "";
            OnPropertyChanged("ReviewContent");

            MessagingCenter.Send<ProductDetailViewModel>(this, result ? "AddReviewSuccess" : "AddReviewFail");
        }

        /// <summary>
        /// Called by the MyModel property to generate extra context based on current MyModel.
        /// </summary>
        private void InitExtraContext()
        {
            // Get product line
            var myProductLine = productLineService.ProductLines.FirstOrDefault(productLine => productLine._id == MyModel.ProductLine);
            ProductLine_Name = myProductLine.Name;

            // Get brand
            var myBrand = brandCatalogService.GetAllBrands().FirstOrDefault(brand => brand._id == myProductLine.Brand);
            Brand_Name = myBrand.LegalName;

            // Get category
            var myCategory = categoryService.Categories.FirstOrDefault(category => category._id == MyModel.Category);
            Category_Name = myCategory.Name;

            //Get reviews
            this.PopulateReviews();
        }

        private void PopulateReviews()
        {
            List<ReviewModel> reviews = reviewService.GetReviewsByProduct(MyModel._id);
            ReviewListViewModel.Refresh(reviews);
        }

        // Utility/Other //----

        // Something about the INotifyPropertyChanged interface...
        public event PropertyChangedEventHandler PropertyChanged;


        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
