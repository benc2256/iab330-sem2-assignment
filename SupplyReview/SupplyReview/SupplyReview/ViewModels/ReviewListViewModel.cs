﻿using SupplyReview.Models;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace SupplyReview.ViewModels
{
    /// <summary>
    /// The view model for displaying reviews.
    /// This is implemented so i can be used in multiple pages when necessary
    /// </summary>
    public class ReviewListViewModel
    {
        public ObservableCollection<ReviewModel> ReviewList { get; set; } //

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="reviews">The list of reviews being set to the view model</param>
        public ReviewListViewModel(ObservableCollection<ReviewModel> reviews)
        {
            ReviewList = reviews;
        }

        /// <summary>
        /// Refreshes the list of reviews to be displayed without reseting the binding context
        /// </summary>
        /// <param name="reviews"> the new list of reviews</param>
        public void Refresh(List<ReviewModel> reviews)
        {
            this.ReviewList.Clear();
            foreach (ReviewModel review in reviews) this.ReviewList.Add(review);
        }
    }
}
