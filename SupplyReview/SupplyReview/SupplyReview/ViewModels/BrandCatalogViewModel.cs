﻿using System;
using System.Collections.Generic;
using System.Text;
using SupplyReview.Models;
using SupplyReview.Services;
using System.Collections.ObjectModel;

namespace SupplyReview.ViewModels
{
    /// <summary>
    /// The view model is used for the binding context for the Brand Catalog Page
    /// </summary>
    public class BrandCatalogViewModel
    {
        public ProductLineListViewModel ProductLineListViewModel { get; set; } //The View model for the list of productlines used in binding context
        public ProductListViewModel ProductListViewModel { get; set; } //The View model for the list of product used in binding context
        public BrandModel Brand { get; set; } //The View model for the brand information used in binding context

        private IBrandCatalogService brandCatalogService; //

        /// <summary>
        /// This initialises the view model for a brand catalog.
        /// It initialises a brand catalog services and then sets the contents of the
        /// brand's products and product-lines
        /// </summary>
        /// <param name="brand">The brand being displayed</param>
        /// <param name="brandService">Dependency to be injected</param>
        public BrandCatalogViewModel(BrandModel brand, IBrandCatalogService brandService)
        {
            //Assign the service based on dependency injection
            brandCatalogService = brandService;

            //Get the lists to be displayed
            ObservableCollection<ProductLineModel> productLines = new ObservableCollection<ProductLineModel>(brandCatalogService.GetProductLinesByBrandID(brand._id));
            ObservableCollection<ProductModel> products = new ObservableCollection<ProductModel>(brandCatalogService.GetProductsByBrandID(brand._id));

            //Instantiate the view models used in binding context
            ProductLineListViewModel = new ProductLineListViewModel(productLines);
            ProductListViewModel = new ProductListViewModel(products);
            Brand = brand;
        }
    }
}
