﻿using SupplyReview.Models;
using SupplyReview.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SupplyReview.ViewModels
{
    /// <summary>
    /// This view model is used for the whole product line index page
    /// </summary>
    public class ProductLineIndexViewModel : INotifyPropertyChanged
    {

        // Public Properties //----

        private string _SearchQuery;
        /// <summary>
        /// Used for searchbar binding and search operations
        /// </summary>
        public string SearchQuery {
            get { return _SearchQuery; }
            set {
                _SearchQuery = value;

                // Get the search results
                var results = productLineService.ProductLines.Where(productLine => productLine.Name.ToLower().Contains(value.ToLower())).ToList();

                // Clear list view then populate with search results
                ListViewModel.RefreshList(results);
            }
        }
        

        /// <summary>
        /// This is the view model for the listview - presentation logic
        /// </summary>
        public ProductLineListViewModel ListViewModel { get; set; }


        /// <summary>
        /// The title of the page
        /// </summary>
        public string PageTitle { get; set; }


        // Private variables //----

        /// <summary>
        /// Service used to interact with db
        /// </summary>
        private IProductLineService productLineService;


        // Constructor //----

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="productLineService">Dependency on service</param>
        public ProductLineIndexViewModel(IProductLineService productLineService)
        {
            // Dependency Injection
            this.productLineService =  productLineService;

            // Init
            var ProductLineList = new ObservableCollection<ProductLineModel>(productLineService.ProductLines); //productLineService.ProductLines
            ListViewModel = new ProductLineListViewModel(ProductLineList); // Property is public so Depend. Inject not needed
            // Default title
            PageTitle = "Product Lines";
        }


        // Public methods //----

        /// <summary>
        /// Set the product line index instance to filter on a brand.
        /// </summary>
        /// <param name="brandID">The id of the brand to filter on</param>
        public void SetFilterOnBrand(string brandID)
        {
            var filteredResults = productLineService.ProductLines.Where(productLine => productLine.Brand == brandID).ToList();

            // Update listings
            ListViewModel.RefreshList(filteredResults);
        }


        /// <summary>
        /// Set the product line index instance to filter on a brand.
        /// </summary>
        /// <param name="brand">The brand to filter on as a model</param>
        public void SetFilterOnBrand(BrandModel brand)
        {
            SetFilterOnBrand(brand._id);
        }


        // Utility/Other //----

        // Something about the INotifyPropertyChanged interface...
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
