﻿using SupplyReview.Models;
using SupplyReview.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SupplyReview.ViewModels
{
    /// <summary>
    /// The view model used as the binding context for the Brand Index page
    /// </summary>
    public class BrandIndexViewModel
    {
        private string _SearchQuery;// The current search query
        
        /// <summary>
        /// Used to update the brand list based on the change in search query
        /// </summary>
        public string SearchQuery
        {
            get { return _SearchQuery; }
            set
            {
                _SearchQuery = value;
                List<BrandModel> allBrands = brandCatalogService.GetAllBrands();
                List<BrandModel> results = allBrands.Where(brands => brands.TradingName.ToLower().Contains(value.ToLower())).ToList();
                ListViewModel.RefreshList(results);
            }
        }

        public BrandListViewModel ListViewModel { get; set; } //The view model for the list of brands

        private IBrandCatalogService brandCatalogService; //the service used to set the list of brands

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="brandService">Dependency Injected</param>
        public BrandIndexViewModel(IBrandCatalogService brandService) 
        {
            brandCatalogService = brandService;
            var BrandList = new ObservableCollection<BrandModel>(brandCatalogService.GetAllBrands());
            ListViewModel = new BrandListViewModel(BrandList);
        }
    }
}
