﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using SupplyReview.Models;

namespace SupplyReview.ViewModels
{
    /// <summary>
    /// This class represents the view model for the "list" of products
    /// so you should be able to reuse this view model in many pages.
    /// </summary>
    public class ProductListViewModel : INotifyPropertyChanged
    {
        // Public Properties //----

        /// <summary>
        /// The list of products to be presented in the view
        /// </summary>
        public ObservableCollection<ProductModel> ProductList { get; set; }


        /// <summary>
        /// Intercepts SelectedProduct binding to figure out what page
        /// to take the user to.
        /// </summary>
        public ProductModel SelectedProduct {
            get {
                return null;
            }
            set {
                if (value != null) {
                    Navigation.PageNavigationManager.Instance.ShowProductDetailPage(value);
                    OnPropertyChanged(nameof(SelectedProduct));
                }
            }
        }


        // Constructor //----

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="productList">The product list to initially display.</param>
        public ProductListViewModel(ObservableCollection<ProductModel> productList)
        {
            this.ProductList = productList;
        }


        // Public methods //---- 

        /// <summary>
        /// Updates the list of items to be displayed to the provided list.
        /// </summary>
        /// <param name="productList">The list to update to.</param>
        public void RefreshList(List<ProductModel> productList)
        {
            this.ProductList.Clear();
            foreach (ProductModel product in productList) this.ProductList.Add(product);
        }


        // Utility/Other //----

        // Something about interface and binding
        public event PropertyChangedEventHandler PropertyChanged;


        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
