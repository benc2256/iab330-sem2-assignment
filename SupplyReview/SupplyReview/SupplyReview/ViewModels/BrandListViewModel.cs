﻿using SupplyReview.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace SupplyReview.ViewModels
{
    /// <summary>
    /// This class is the implmentation of the view model used
    /// to bind a list of brands to a view
    /// It has been implmented so that it can be displayed in various pages when
    /// necessary.
    /// </summary>
    public class BrandListViewModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged; //Required for implmentation of interface


        public ObservableCollection<BrandModel> BrandList { get; set; } //The brand list to be used in the view

        /// <summary>
        /// The brand selected in the list. It is bound to the SelectedItem property of the 
        /// list through the use of 2 way binding.
        /// When the value is changed to a non null entry the page, the page navigation manager is used to
        /// load the brand catalog page for the selected brand
        /// </summary>
        public BrandModel SelectedBrand
        {
            get
            {
                return null;
            }
            set
            {
                if (value != null)
                {

                    Navigation.PageNavigationManager.Instance.ShowBrandCatalogPage(value);
                    OnPropertyChanged(nameof(SelectedBrand));
                }
            }
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="BrandList">The initial list of brands to be displayed</param>
        public BrandListViewModel(ObservableCollection<BrandModel> BrandList)
        {
            this.BrandList = BrandList;
        }
        
        /// <summary>
        /// Used to detect the change of the selected brand 
        /// </summary>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Updates the list of the brands being displayed without resetting the binding context
        /// for the view
        /// </summary>
        /// <param name="brandList">The new list to be displayed</param>
        public void RefreshList(List<BrandModel> brandList)
        {
            this.BrandList.Clear();
            foreach (BrandModel brand in brandList) this.BrandList.Add(brand);
        }
    }
}
