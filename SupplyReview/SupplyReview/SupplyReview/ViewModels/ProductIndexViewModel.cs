﻿using SupplyReview.Models;
using SupplyReview.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

using SupplyReview.Core;
using System.Linq;

namespace SupplyReview.ViewModels
{

    /// <summary>
    /// This view model is used for the whole product index page
    /// </summary>
    public class ProductIndexViewModel : INotifyPropertyChanged
    {
        //Public Properties//----

        private string _SearchQuery;
        /// <summary>
        /// Used for searchbar binding and search operations
        /// </summary>
        public string SearchQuery
        {
            get { return _SearchQuery; }
            set {
                _SearchQuery = value;

                ProductModel[] products = Search.GetProductsByName(this.productService.GetProducts(), value);

                ListViewModel.RefreshList(products.ToList());
            }
        }


        /// <summary>
        /// This is the view model for the listview - presentation logic
        /// </summary>
        public ProductListViewModel ListViewModel { get; set; }


        /// <summary>
        /// The title of the page
        /// </summary>
        public string PageTitle { get; set; }


        // Private variables //----

        /// <summary>
        /// Service used to interact with db
        /// </summary>
        private IProductService productService;


        // Constructor //----

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="productService">Dependency on service</param>
        public ProductIndexViewModel(IProductService productService)
        {
            // Dependency Injection
            this.productService = productService;

            // Init
            var ProductLineList = new ObservableCollection<ProductModel>(productService.GetProducts());
            ListViewModel = new ProductListViewModel(ProductLineList);
        }


        // Public Methods //----

        /// <summary>
        /// Set the product line index instance to filter on a product line.
        /// </summary>
        /// <param name="productLineID">The id of the product line to filter on</param>
        public void SetFilterOnProductLine(string productLineID)
        {
            var filteredResults = productService.GetProducts().Where(product => product.ProductLine == productLineID).ToList();

            // Update listings
            ListViewModel.RefreshList(filteredResults);
        }


        /// <summary>
        /// Set the product line index instance to filter on a product line.
        /// </summary>
        /// <param name="productLineModel">The product line to filter on as a model</param>
        public void SetFilterOnProductLine(ProductLineModel productLineModel)
        {
            SetFilterOnProductLine(productLineModel._id);
        }


        // Utility/Other //----

        // Something about the INotifyPropertyChanged interface...
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
