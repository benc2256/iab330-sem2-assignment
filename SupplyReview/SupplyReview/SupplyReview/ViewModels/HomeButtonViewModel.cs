﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SupplyReview.ViewModels
{
    public class HomeButtonViewModel
    {
        public ICommand HomeButtonCommand { protected set; get; }

        public HomeButtonViewModel()
        {
            HomeButtonCommand = new Command(() =>
            {
                Navigation.PageNavigationManager.Instance.ShowHomePage();
            });
    
        }
    }
}
