﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SupplyReview.ViewModels
{
    class HomePageViewModel
    {
        // Command properties to bind to
        public ICommand NavigateToBrandIndexCommand { get; private set; }
        public ICommand NavigateToProductLineIndexCommand { get; private set; }
        public ICommand NavigateToProductIndexCommand { get; private set; }

        public HomePageViewModel() {
            // Assign Action to run when command is called.
            NavigateToBrandIndexCommand = new Command(() => Navigation.PageNavigationManager.Instance.ShowBrandIndexPage());
            NavigateToProductLineIndexCommand = new Command(() => Navigation.PageNavigationManager.Instance.ShowProductLineIndexPage());
            NavigateToProductIndexCommand = new Command(() => Navigation.PageNavigationManager.Instance.ShowProductListIndexPage());
        }
    }
}
