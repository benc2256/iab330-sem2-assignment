﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SupplyReview.ViewModels;
using SupplyReview.Extensions.Xaml;

namespace SupplyReview.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProductDetailPage : TabbedPage
	{
		public ProductDetailPage ()
		{
			InitializeComponent ();

            MessagingCenter.Subscribe<ProductDetailViewModel>(this, "AddReviewSuccess", (sender) => {
                DisplayAlert("Review Posted", "Your review has been posted.", "Ok");
            });
            MessagingCenter.Subscribe<ProductDetailViewModel>(this, "AddReviewFail", (sender) => {
                DisplayAlert("Unable to Post", "Failed to post review.\nPlease try again later.", "Ok");
            });
        }
	}
}